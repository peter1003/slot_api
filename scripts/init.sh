#!/bin/bash
# Install swag doc generate
go get -u github.com/swaggo/swag/cmd/swag
# Install Migrate
version=v4.12.2
curl -L https://github.com/golang-migrate/migrate/releases/download/$version/migrate.linux-amd64.tar.gz | tar xvz && mv migrate.linux-amd64 migrate 
mv migrate /go/bin/migrate
go get -u github.com/smallnest/gen
