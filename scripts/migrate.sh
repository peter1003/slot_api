#!/bin/bash
#
#
function create() {
	cd ../
	migrate create -ext sql -dir src/migrations -seq "$@"
}

function up() {
  cd ../
  migrate -source file://src/migrations -database mysql://$DB_USERNAME:$DB_PASSWORD@tcp\($DB_HOST:$DB_PORT\)/$DB_DATABASE?query up
}

function down() {
  cd ../
  migrate -source file://src/migrations -database mysql://$DB_USERNAME:$DB_PASSWORD@tcp\($DB_HOST:$DB_PORT\)/$DB_DATABASE?query down 1
}
usage="$(basename "$0") [-h] [-s n] -- program to calculate the answer to life, the universe and everything

where:
    -h  show this help text
    up  up 
    create create new migrate file
    "

seed=42
while getopts ':hs:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done

shift $((OPTIND - 1))
if [ "$(type -t $1)" = 'function' ]; then
    $1 "$@"
    else
    	echo "Function Not Found"
fi



#cd ../


#migrate -source file://src/migrations -database mysql://$DB_USERNAME:$DB_PASSWORD@tcp\($DB_HOST:$DB_PORT\)/$DB_DATABASE?query up 

