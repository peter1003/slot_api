package slot

import (
	cr "codes.slot_api/src/errors"
	logic "codes.slot_api/src/library/slot_logic"
	"fmt"
)

func DoSlot(request *SlotRequest) *cr.CustomError {
	fmt.Println(logic.GetAllGames())
	Game, _ := logic.GetGame(request.GameId)
	fmt.Println(Game)
	fmt.Println(Game.Validate(request))
	return nil
}
