package slot

type SlotRequest struct {
	Ptype   string `form:"ptype" validate:"required" comment:"game.ptype"  `
	Wallet  string `form:"wallet" validate:"required" comment:"game.wallet"`
	GameId  string `form:"game_id" validate:"required" comment:"game.game_id" json:"game_id" `
	Version string `form:"version" validate:"required" comment:"game.version"`
}
