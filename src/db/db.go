package db

import (
	"codes.slot_api/src/utils/config"
	"codes.slot_api/src/utils/logger"
	"codes.slot_api/src/utils/redis"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Options map[string]interface{}
type NullType byte

const (
	_ NullType = iota
	// IsNull the same as `is null`
	IsNull
	// IsNotNull the same as `is not null`
	IsNotNull
)

var (
	lock     sync.Mutex
	db       *gorm.DB
	interval = config.GetInt("db.interval")
	dialect  = config.GetString("db.dialect")
	source   = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s",
		config.GetString("db.user"),
		config.GetString("db.password"),
		config.GetString("db.host"),
		config.GetString("db.port"),
		config.GetString("db.database"),
		config.GetString("db.flag"),
	)
)

func init() {
	go connectionPool()
}

// DB return database instance
func DB() *gorm.DB {
	if db != nil {
		return db
	}

	lock.Lock()
	defer lock.Unlock()

	if db != nil {
		return db
	}

	if db == nil {
		connect(dialect, source)
	}
	db.LogMode(config.GetBool("db.isShowConsole"))
	return db
}

// Close close database connection
func Close() {
	if db != nil {
		err := db.Close()
		if err != nil {
			logger.ApplicationErrorSimple("gorm close error", err)
		}
	}
}

func connect(dialect string, source string) {
	dbInstance, err := gorm.Open(dialect, source)

	if err != nil {
		logger.ApplicationErrorSimple("gorm connection open error", err)
	} else {
		dbInstance.SingularTable(true)
		dbInstance.BlockGlobalUpdate(true)
		dbInstance.DB().SetConnMaxLifetime(time.Duration(interval) * time.Second)
		dbInstance.DB().SetMaxIdleConns(200)
		dbInstance.DB().SetMaxOpenConns(200)
		db = dbInstance
	}
}

func connectionPool() {
	for {
		if err := DB().DB().Ping(); err != nil {
			logger.ApplicationErrorSimple("Error ping to DB", err)
			if db != nil {
				dbErr := db.Close()
				if dbErr != nil {
					logger.ApplicationErrorSimple("close db after ping db error", dbErr)
				}
			}
			connect(dialect, source)
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}

// Repository
func List(tableName string, rows interface{}, options Options) (int64, error) {
	search, _ := options["search"].(Options)
	page, _ := options["page"].(int64)
	perpage, _ := options["perpage"].(int64)
	if page == 0 {
		page = int64(1)
	}
	if perpage == 0 {
		perpage = int64(30)
	}
	sort, _ := options["sort"].(string)
	cond, vals, err := WhereBuild(search)

	if err != nil {
		return 0, err
	}

	var count int64
	//設定快取
	enableCache := config.GetBool("db.enableCache")
	cacheKey := CacheKey(tableName, page, search, perpage, sort)
	countCacheKey := cacheKey + "|count"
	if enableCache && false {
		redisResultJsonString, _ := redis.GetBytes(cacheKey)
		countRedisString, _ := redis.Get(countCacheKey)
		if len(redisResultJsonString) == 0 || countRedisString == "" {
			if err := DB().Table(tableName).Where(cond, vals...).Order(sort).Offset(page * perpage).Scan(rows).Count(&count).Error; err != nil {
				return 0, err
			}
			jsonString, _ := json.Marshal(rows)
			redis.SetEx(cacheKey, config.GetInt("db.ttl"), jsonString)
			redis.SetEx(countCacheKey, config.GetInt("db.ttl"), strconv.FormatInt(int64(count), 10))
		} else {
			json.Unmarshal(redisResultJsonString, &rows)
			count, _ = strconv.ParseInt(countRedisString.(string), 10, 64)
		}
	} else {
		if err := DB().Table(tableName).Where(cond, vals...).Order(sort).Offset(page * perpage).Scan(rows).Count(&count).Error; err != nil {
			return count, err
		}
	}

	return count, nil
}
func Get(tableName string, rows interface{}, id string) error {
	//設定快取
	enableCache := config.GetBool("db.enableCache")

	cacheKey := CacheKey(tableName, id)

	countCacheKey := cacheKey + "|1"
	if enableCache {
		redisResultJsonString, _ := redis.GetBytes(cacheKey)
		countRedisString, _ := redis.Get(countCacheKey)
		if len(redisResultJsonString) == 0 || countRedisString == "" {
			if err := DB().Table(tableName).Where("id = ?", id).Scan(rows).Error; err != nil {
				return err
			}
			jsonString, _ := json.Marshal(rows)
			// redis.SetEx(cacheKey, config.GetInt("db.ttl"), string(jsonString))
			redis.SetEx(cacheKey, config.GetInt("db.ttl"), string(jsonString))
		} else {
			json.Unmarshal(redisResultJsonString, rows)
		}
	} else {
		fmt.Println("WTFgg")
		if err := DB().Table(tableName).Where("id = ?", id).Scan(rows).Error; err != nil {
			return err
		}
	}
	return nil
}

func Save(tableName string, current interface{}, values Options) {
	tx := DB()
	if id, ok := values["id"]; ok {
		fmt.Println("B")
		if id != "" {
			tx.Model(current).Updates(values).Where("id=?", id)
		} else {
			tx.FirstOrCreate(current)
		}
	} else {
		tx.Table(tableName).Save(current)
		tx.Model(current).Updates(values)
	}
	//設定快取
	enableCache := config.GetBool("db.enableCache")
	// fmt.Println("ENABLE")
	// fmt.Println(enableCache)
	if enableCache {
		//刪除快取
		cacheKeyPerfix := CacheKey(tableName + "*")
		redis.DelByPattern(cacheKeyPerfix)
	}
}

func Delete(tableName string, current interface{}) error {
	//db.Delete(&User{}, 10)
	// err := DB().Table(tableName).Delete(current, where).Error
	err := DB().Delete(current).Error
	return err
}

func CacheKey(prefix string, vals ...interface{}) string {
	str := prefix
	if len(vals) > 0 {
		for _, v := range vals {
			str = str + "|" + fmt.Sprint(v)
		}
	}
	str = config.GetString("db.cachePrefix") + str
	return str
}

//WhereBuild sql build where

func WhereBuild(where map[string]interface{}) (whereSQL string, vals []interface{}, err error) {
	for k, v := range where {
		ks := strings.Split(k, " ")
		if len(ks) > 2 {
			return "", nil, fmt.Errorf("Error in query condition: %s. ", k)
		}

		if whereSQL != "" {
			whereSQL += " AND "
		}
		strings.Join(ks, ",")
		switch len(ks) {
		case 1:
			//fmt.Println(reflect.TypeOf(v))
			switch v := v.(type) {
			case NullType:
				if v == IsNotNull {
					whereSQL += fmt.Sprint(k, " IS NOT NULL")
				} else {
					whereSQL += fmt.Sprint(k, " IS NULL")
				}
			default:
				whereSQL += fmt.Sprint(k, "=?")
				vals = append(vals, v)
			}
			break
		case 2:
			k = ks[0]
			switch ks[1] {
			case "=":
				whereSQL += fmt.Sprint(k, "=?")
				vals = append(vals, v)
				break
			case ">":
				whereSQL += fmt.Sprint(k, ">?")
				vals = append(vals, v)
				break
			case ">=":
				whereSQL += fmt.Sprint(k, ">=?")
				vals = append(vals, v)
				break
			case "<":
				whereSQL += fmt.Sprint(k, "<?")
				vals = append(vals, v)
				break
			case "<=":
				whereSQL += fmt.Sprint(k, "<=?")
				vals = append(vals, v)
				break
			case "!=":
				whereSQL += fmt.Sprint(k, "!=?")
				vals = append(vals, v)
				break
			case "<>":
				whereSQL += fmt.Sprint(k, "!=?")
				vals = append(vals, v)
				break
			case "in":
				whereSQL += fmt.Sprint(k, " in (?)")
				vals = append(vals, v)
				break
			case "like":
				whereSQL += fmt.Sprint(k, " like ?")
				vals = append(vals, v)
			}
			break
		}
	}
	return
}
