package httputil

import (
 "github.com/gin-gonic/gin"
 cr "codes.slot_api/src/errors"
)


// NewError example
func NewError(ctx *gin.Context, err *cr.CustomError) {
	er := HTTPError{
		Code:    err.Code,
		Message: err.Msg,
		Data : err.Data,
	}
	ctx.JSON(500, er)
}

// HTTPError example
type HTTPError struct {
	Code    string    `json:"code" example:"400"`
	Message string `json:"message" example:"status bad request"`
	Data interface{} `json:"data"`
}

func JSON(ctx *gin.Context, status int, data interface{} , message string) {
	response := HTTPResponse {
		Code: status,
		Message :message,
		Data : data,
	}
	ctx.JSON(status, response)
}


type HTTPResponse struct {
	Code    int    `json:"code" example:"200"`
	Data    interface{}    `json:"data" `
	Message string `json:"message" example:"status bad request"`
}

