package slot_logic

type Slot interface {
	Model() interface{} //对应的ORM 物件
	Validate(interface{}) bool
	Result(interface{}) interface{}
}

var (
	allGames map[string]Slot
)

/**
 * 加入到游戏池子里去
 * @param  string 游戏识别名
 * @param  Slot   游戏
 * @return nil
 */
func appendAllGames(gameName string, game Slot) {
	allGames[gameName] = game
}

func init() {
	allGames = map[string]Slot{}
}
func GetGame(gameName string) (Slot, bool) {
	game, ok := allGames[gameName]
	return game.(Slot), ok
}

func GetAllGames() map[string]Slot {
	return allGames
}
