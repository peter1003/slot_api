package slot_logic

import (
	"codes.slot_api/src/models"
)

type HighWayKing struct {
	slot Slot
}

func (s *HighWayKing) Validate(interface{}) bool {
	return true
}
func (s *HighWayKing) Model() interface{} {
	m := models.Hwk{}
	return m
}
func (s *HighWayKing) Result(interface{}) interface{} {
	var t interface{}
	t = "HighWayKing"
	return t
}

func init() {
	t := new(HighWayKing)
	appendAllGames("HighWayKing", t)
}
