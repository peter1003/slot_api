package errors

import (
  "github.com/pkg/errors"
)
const (
	Custom ="501"
)
type CustomError struct {
	Msg string `json:"message"`
	Code string `json:"code"`
	Err  error `json:"-"`
	Data interface{} `json:"data"`
}

func Error (err error, data interface{}) *CustomError {
	if data == nil {
		data = make([]int,0)
	}
	return &CustomError{
			Msg: err.Error(),
			Code : Custom,
			Err : errors.Wrap(err, "custom_error"),
			Data : data,
	}
}	

// Create a function Error() string and associate it to the struct.
func(error *CustomError) Error() string {
    return error.Msg
}
func(error *CustomError) TrueError() error {
    return error.Err
}
