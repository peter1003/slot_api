package web

import (
	"bitbucket.org/peter1003/common/src/utils/config"
	validator "bitbucket.org/peter1003/common/src/utils/validator"
	"codes.slot_api/docs"
	// "codes.slot_api/src/utils/redis"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
)

// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host codes.slotapi:8081
// @BasePath /api/v1
// @query.collection.format multi

// @securityDefinitions.basic BasicAuth

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

// @securitydefinitions.oauth2.application OAuth2Application
// @tokenUrl https://example.com/oauth/token
// @scope.write Grants write access
// @scope.admin Grants read and write access to administrative information

// @securitydefinitions.oauth2.implicit OAuth2Implicit
// @authorizationurl https://example.com/oauth/authorize
// @scope.write Grants write access
// @scope.admin Grants read and write access to administrative information

// @securitydefinitions.oauth2.password OAuth2Password
// @tokenUrl https://example.com/oauth/token
// @scope.read Grants read access
// @scope.write Grants write access
// @scope.admin Grants read and write access to administrative information

// @securitydefinitions.oauth2.accessCode OAuth2AccessCode
// @tokenUrl https://example.com/oauth/token
// @authorizationurl https://example.com/oauth/authorize
// @scope.admin Grants read and write access to administrative information

// @x-extension-openapi {"example": "value on a json format"}
func StartWeb() {
	r := gin.New()

	r.Use(gin.Logger())

	SetupRouter(r)
	// ls.Apply("jp")

	validator.Boot("../../lang/validator", "en")
	if config.GetString("server.mode") != gin.ReleaseMode { //Production不能看到API文件
		Doc(r)
	}
	r.Run(fmt.Sprintf("%s:%s", config.GetString("server.host"), config.GetString("server.port")))
	// defer redis.Close()
}

func Doc(r *gin.Engine) {
	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Slot API"
	docs.SwaggerInfo.Description = "Slog API Document"
	docs.SwaggerInfo.Version = "1.0"
	// docs.SwaggerInfo.Host = config.GetString("server.host")
	// docs.SwaggerInfo.Port = config.GetString("server.port")
	// docs.SwaggerInfo.BasePath = config.GetString("api.v1.prefix")
	docs.SwaggerInfo.Schemes = []string{"http", "https"}
	// use ginSwagger middleware to serve the API docs
	r.GET("/doc/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

}
