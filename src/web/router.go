package web

import (
	"bitbucket.org/peter1003/common/src/utils/config"
	"github.com/gin-gonic/gin"
)

func SetupRouter(r *gin.Engine) *gin.Engine {

	group := r.Group(config.GetString("api.v1.prefix"))
	group.GET("/test", Test)
	group.POST("/slot", Slot)

	return r
}
