package web

import (
	model "bitbucket.org/Robert922/game_model/src/model"

	cr "bitbucket.org/peter1003/common/src/utils/redis"
	db "bitbucket.org/peter1003/common/src/utils/repository"
	validator "bitbucket.org/peter1003/common/src/utils/validator"
	"codes.slot_api/src/errors"
	"codes.slot_api/src/services/slot"
	"codes.slot_api/src/utils/httputil"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"

	"strconv"
	"time"
)

// Test godoc
// @Summary Test Serive Is alive
// @Description Test Serive Is alive
// @Accept  json
// @Produce  json
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /test [get]
func Test(ctx *gin.Context) {
	//測試Redis
	// RedisTest()
	// dbTest()

	httputil.JSON(ctx, 200, nil, "success")
}

func dbTest() {
	log := model.Agents{}
	queryResult := []model.Agents{}
	search := db.Options{}
	// search["deleted"] = model.NOT_DELETED
	var options = map[string]interface{}{
		"search":  search,
		"page":    1,
		"perpage": 100,
		// "sort":    repo.GetSort(sort, log.SortMap()),
		"sort": "id desc",
	}
	db.List(log.TableName(), &queryResult, options)
	fmt.Println("list")
	fmt.Println(queryResult)
	t := time.Now()
	var inputValues = map[string]interface{}{
		"name":       "test" + t.String(),
		"is_enabled": 1,
	}
	db.Save(log.TableName(), &log, inputValues)
	queryLog := model.Agents{}
	db.Get(log.TableName(), &queryLog, strconv.FormatUint(log.ID, 10))
	db.Delete(log.TableName(), &log)
}

func RedisTest() {
	redisKey := "redisKey"
	defer cr.Close()
	// resSetValue, err := redis.String(redisClient.Do("setex", redisKey, 600, 1))
	resSetValue, err := redis.String(cr.Do("setex", redisKey, 600, 1))
	if err != nil {
		msg := fmt.Sprintf("fail to set redis key=%s, error:%s", redisKey, err.Error())
		fmt.Println(msg)
	} else {
		msg := fmt.Sprintf("succeed to set redis key=%s, value=%#v", redisKey, resSetValue)
		fmt.Println(msg)
	}
	redisValue, err := redis.String(cr.Do("GET", redisKey))
	if err != nil {
		msg := fmt.Sprintf("fail to get content by redis key=%s, error:%s", redisKey, err.Error())
		fmt.Println(msg)
	} else {
		msg := fmt.Sprintf("succeed to get content by redis key=%s, value=%#v", redisKey, redisValue)
		fmt.Println(msg)
	}

	strRedisValue, err := redis.String(cr.Do("PING", nil))
	if err != nil {
		msg := fmt.Sprintf("fail to ping, error:%s", err.Error())
		fmt.Println(msg)
	} else {
		msg := fmt.Sprintf("succeed to ping, value=%#v", strRedisValue)
		fmt.Println(msg)
	}
}

// Slot_Godofwealth2 godoc
// @Summary Slot_Godofwealth2
// @Description Slot_Godofwealth2
// @Produce  json
// @Param game_id formData string true "Game Id : slot_xxxx , roulette_36"
// @Param ptype formData string true "pointa,pointb"
// @Param wallet formData string true "singleplayer_games"
// @Param version formData string true "1000.0.2"
// @Header 200 {string} Token "qwerty"
// Success 200 {object} model.Account
// @Failure 400 {object} httputil.HTTPError
// @Failure 404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /slot [post]
func Slot(ctx *gin.Context) {
	slotRequestBody := new(slot.SlotRequest)
	// if err := ctx.ShouldBindBodyWith(slotRequestBody); err != nil {
	// 	httputil.NewError(ctx, errors.Error(err, nil))
	// 	return
	// }
	if err := validator.Validate(ctx, slotRequestBody); err != nil {
		httputil.NewError(ctx, errors.Error(err, nil))
		return
	}
	fmt.Println(slotRequestBody)
	serviceErr := slot.DoSlot(slotRequestBody)
	if serviceErr != nil {
		httputil.NewError(ctx, serviceErr)
	}
	httputil.JSON(ctx, 200, nil, "success")
}
