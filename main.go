package main

import (
	_ "bitbucket.org/peter1003/common/src/utils/config"
	"codes.slot_api/src/web"
)

func main() {
	web.StartWeb()
}
