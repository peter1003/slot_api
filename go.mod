module codes.slot_api

go 1.15

require (
	bitbucket.org/Robert922/game_model v0.0.0-20200907054241-a9b34d4c17c4
	bitbucket.org/peter1003/common v0.0.0-20200910051539-80be73d49758
	cloud.google.com/go/datastore v1.1.0 // indirect
	cloud.google.com/go/pubsub v1.3.1 // indirect
	cloud.google.com/go/storage v1.11.0 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/census-instrumentation/opencensus-proto v0.3.0 // indirect
	github.com/cncf/udpa/go v0.0.0-20200629203442-efcf912fb354 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/enriquebris/goconcurrentqueue v0.6.0
	github.com/envoyproxy/go-control-plane v0.9.6 // indirect
	github.com/envoyproxy/protoc-gen-validate v0.4.1 // indirect
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gin-contrib/gzip v0.0.2 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-openapi/spec v0.19.9 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/gomodule/redigo v1.8.2
	github.com/google/gofuzz v1.2.0 // indirect
	github.com/iancoleman/strcase v0.1.1 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20200824232613-28f6c0f3b639 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/masnun/gopher-and-rabbit v0.0.0-20190223090227-19d477901766
	github.com/mylukin/easy-i18n v0.0.0-20200903102537-d4a15e952a3e
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/pkg/sftp v1.12.0 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/spf13/afero v1.3.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.7
	github.com/urfave/cli/v2 v2.2.0 // indirect
	github.com/yadvendar/redigo-wrapper v0.0.0-20170501123926-fdb6f41c34bd
	go.uber.org/zap v1.10.0
	golang.org/x/exp v0.0.0-20200224162631-6cc2880d07d6 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	golang.org/x/sys v0.0.0-20200831180312-196b9ba8737a // indirect
	golang.org/x/text v0.3.3
	golang.org/x/tools v0.0.0-20200903005429-2364a5e8fdcf // indirect
	google.golang.org/api v0.31.0 // indirect
	google.golang.org/genproto v0.0.0-20200903010400-9bfcb5116336 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
