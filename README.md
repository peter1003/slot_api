# Configure #

# Modify docker-compose.yaml

		docker-compose.yaml

# Build 

		docker-compose build 

# First Run 

## Install Need Tool Package 

	docker-compose run dev bash -c "scripts/init.sh"


# Running on development


	docker-compose up -d db dev

# Running on Production

	docker-compose up -d db production

# Access API 

	
		http://0.0.0.0:8081/api/


# API Doc Generate&Refresh

	docker-compose exec dev swag init -g src/web/main.go

# Access API Doc  (Production Can't  access doc )


	http://0.0.0.0:8081/swagger/index.html




